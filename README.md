# ac-computadora

Mapas conceptuales de los temas de Von-Neumann vs Harvard y las supercomputadoras en México

## Von-Neumann vs Harvard

```plantuml

@startmindmap

<style>
     .rootnode{

     }
     .parrafo{
          Padding 25
          Margin 1
          HorizontalAlignment center
          LineThickness 3.0
          RoundCorner 40
          MaximumWidth 176
     }
</style>


caption Mapa Conceptual
title "Von-Neumann vs Harvard"

center footer Angel Jesus Zorrilla Cuevas Grupo: ISB 



*[#50fa7b] Arquitectura de Computadoras 
     ** John Von-Neumann (1903-1957) \nfisico matemático \nhúngaro-estadounidese.  
          
          *** -Pionero en las ciencias de la computación.\n-Creó la arquitectura de las computadoras actuales. \n-Resolvió el problema de la obtención de respuestas fiables \n con componentes infiables. \n-Propuso separar el hardware del software.
               
     ** Modelos de arquitectura \nclásica
          *** Modelo Von-Neumann
               
               **** Tuvo su origen en 1945 cuando John Von-Neuman junto con John Mauchly y Eckert modificaron la ENIAC con una sola unidad de almacenamiento <<parrafo>>

               **** Que consiste en diferentes \nbloques funcionales que conforman \nuna computadora.
                    *****_ CPU
                         ****** Encargado de realizar \nlas operaciones básicas y de gestionar \nel funciomamiento de los \ndemás componentes.
                    *****_ Memoria 
                         ****** Lugar en el que se almacenan \nlos datos y las instruicciones.
                    *****_ Buses
                         ****** Conexionado que permite la \ncomunicación entre los recursos \ndel sistema
                    *****_ Periféricos
                         ****** Instrumentos de entrada \ny salida de datos.
               ****_ Propiedades
                    ***** Unico espacio de memoria
                    ***** El contenido de la memoria \nes accesible por posición.
                    ***** La ejecución de instrucciones\n es secuencial

          *** Modelo Harvard
               **** Toma su nombre de la computadora Harvard mark creada por el científico Howard H. Aiken. <<parrafo>>
                    ***** Que tenía dos memorias conectadas al procesador donde una contenía los datos y la otra memoria las instruicciones. <<parrafo>>
          
@endmindmap
```

## Supercomputadoras en México

```plantuml

@startmindmap

<style>
     .parrafo1{
          LineThickness 3.0
          RoundCorner 40
          MaximumWidth 200  
     }
     .parrafo2{
          MaximunWidth 230
     }
</style>


caption Mapa Conceptual
title "Supercomputadoras en México"

center footer Angel Jesus Zorrilla Cuevas Grupo: ISB 

*[#50fa7b] Supercomputadoras en México
     ** Una supercomputadora es una infraestructura que se diseña y construye para procesar grandes cantidades de datos o información, la simulación y búsqueda de patrones. <<parrafo1>>
          *** El supercomputo contribuye al estudio de la estructura del universo, de sismos, y el comportamiento de partículas subatómicas, así como, el diseño de nuevos materiales, farmacos y reactores nucleares. <<parrafo1>>

     **_ Historia de la computación en México
          *** IBM-650 (1958)
               ****_ Primera gran computadora en México
               ****_ Tenía una memoria de 2k y funcionaba con bulbos
               ****_ Leía información a través de tarjetas perforadas
               ****_ Era capaz de procesar 1000 operaciones aritméticas en un segundo.

          *** CRAY YMP 432 (1991)
               ****_ La UNAM adquiere la primer supercomputadora de america latina.
               ****_ Equivalía a 2000 computadoras de oficina
               ****_ Se utilizó para apoyar la investigación en química, ingeniería estructural, astronomía, física e investigaciones del medio ambiente.<<parrafo2>>
               ****_ Consistía en 7000 microcomputadores, una centena de estaciones de trabajo y 3 mainframes dedicados a la docencia y la investigación.<<parrafo2>>

          *** KanBalam (2008)
               ****_ Supercomputadora con una capacidad equivalente a más de 1500 de computadoras de escritorio
               ****_ Puede realizar 7 billones de operaciones matemáticas por segundo.
               ****_ Se utilizó para realizar proyectos e investigaciones en áreas como astronomía, química cuántica, nanotecnología, entre otros.<<parrafo2>>

          *** Miztli (2012)
               ****_ Tiene 8344 procesadores para solucionar problemas relacionados con la ciencia e investigación
               ****_ Cuenta con 58 servidores que aportan 1800 servidores
               ****_ Tiene una velocidad de procesamiento de aproximadamente 228 teraflops de operacions por segundo

          *** Xiuhcoatl (2012)
               ****_ Fue una iniciativa de la UNAM, UAM y Cinvestav para crear un laboratorio de cómputo de alto desempeño
               ****_ Es un cluster híbrido de supercómputo conectado a las 3 instituciones educativas a través de una fibra óptica de 10Gbps de velocidad. <<parrafo2>>
               ****_ Fue la segunda computadora más rápida de Latinoamérica
               ****_ En el 2016 tenía un rendimiento de 252 teraflops.

          *** Supercomputadora de la BUAP (2018)
               ****_ Una de las 5 supercomputadoras más poderosas de Latinoamérica
               ****_ Tiene una capacidad de almacenamiento de casi 5 mil computadoras portátiles
               ****_ En un segundo puede ejecutar hasta 2 billones de operaciones
               ****_ Se utiliza para realizar cálculos complejos que requieren el análisis de miles de millones de datos.<<parrafo2>>



@endmindmap
```